import Vue from 'vue'
import VueRouter from 'vue-router'

require('./bootstrap');

window.events = new Vue();
window.flash = function (message, level = 'success') {
    window.events.$emit('flash', {message, level});
};

Vue.use(VueRouter)

const newsList = require('./components/newsList.vue').default
const newsItem = require('./components/newsItem.vue').default
const newsEdit = require('./components/newsEdit.vue').default

Vue.component('flash', require('./components/flash.vue').default);

const routes = [
    {
        path: '/',
        component: newsList,
    },
    {
        path: '/create',
        component: newsEdit,
    },
    {
        path: '/:link',
        component: newsItem,
    },
    {
        path: '/edit/:link',
        component: newsEdit,
    },
]

const router = new VueRouter({
    routes // сокращённая запись для `routes: routes`
})

const app = new Vue({
    el: '#app',
    router: router,
});
