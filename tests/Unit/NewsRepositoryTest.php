<?php

namespace Tests\Unit;

use App\Core\Contract\Entity;
use App\Core\Contract\EntityCollection;
use App\Models\News\NewsEntity;
use App\Models\News\NewsRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Tests\TestCase;

/**
 * It should be created and used seeders, etc in a proper way
 *
 * @package Tests\Unit
 */
class NewsRepositoryTest extends TestCase
{
    use RefreshDatabase;

    public function testCreateEntity()
    {
        $repository = $this->createRepository();
        $entity = $repository->createNewEntity();

        $this->assertInstanceOf(NewsEntity::class, $entity, 'It was created appropriate entity object');
        $this->assertNull($entity->id, 'The entity is new and does not have the primary key value');
        $this->assertSame(0, $entity->clicks_count, 'The count of clicks is set to 0');
    }

    /**
     * The test check that the getting entity by primary key works fine
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \PHPUnit\Framework\Exception
     * @throws \PHPUnit\Framework\ExpectationFailedException
     * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
     */
    public function testGetByPk(): void
    {
        $newsId = 1;
        $newsTitle = "first new";
        $newsLink = "first-new";

        $repository = $this->createRepository();
        $newsEntity = $repository->getByPk($newsId);

        $this->assertNull($newsEntity, 'It should be returned NULL because there is no records in the database');

        DB::insert('INSERT INTO news (id, title, link) VALUES (:id, :title, :link)', [
            'id' => $newsId,
            'title' => $newsTitle,
            'link' => $newsLink,
        ]);
        $newsEntity = $repository->getByPk($newsId);

        $this->assertInstanceOf(Entity::class, $newsEntity, 'It was created appropriate entity object');
        $this->assertEquals($newsId, $newsEntity->id, 'The entity contains correct id');
        $this->assertEquals($newsTitle, $newsEntity->title, 'The entity contains correct title');
        $this->assertEquals($newsLink, $newsEntity->link, 'The entity contains correct link');
    }

    public function testGetList()
    {
        for ($i = 1; $i < 3; $i++) {
            DB::insert('INSERT INTO news (id, title, link) VALUES (:id, :title, :link)', [
                'id' => $i,
                'title' => 'title_' . $i,
                'link' => 'link-' . $i,
            ]);
        }

        $repository = $this->createRepository();

        $listOfNews = $repository->getList();
        $this->assertInstanceOf(EntityCollection::class, $listOfNews, 'It was created a collection');

        $i = 1;
        foreach ($listOfNews as $newsEntity) {
            $this->assertInstanceOf(Entity::class, $newsEntity, 'It was created appropriate entity object');
            $this->assertEquals($i, $newsEntity->id, 'The entity contains correct id');
            $this->assertEquals("title_" . $i, $newsEntity->title, 'The entity contains correct title');
            $this->assertEquals("link-" . $i, $newsEntity->link, 'The entity contains correct link');

            $i++;
        }
    }

    public function testInsert()
    {
        $repository = $this->createRepository();

        /** @var NewsEntity $entity */
        $entity = $repository->createNewEntity();

        $entity->title = 'test title';
        $entity->content = 'test content';

        $repository->insert($entity);

        $this->assertNotNull($entity->id, 'It was assigned the entity primary key value');

        /** @var NewsEntity $databaseEntity */
        $databaseEntity = $repository->getByPk($entity->id);
        $this->assertEquals($entity->title, $databaseEntity->title);
        $this->assertEquals($entity->content, $databaseEntity->content);
        $this->assertEquals($entity->clicks_count, $databaseEntity->clicks_count);
        $this->assertEquals($entity->link, Str::slug($entity->title));

        /** @var NewsEntity $entity */
        $secondEntity = $repository->createNewEntity();

        $secondEntity->title = 'test title';
        $secondEntity->content = 'test content of the second entity';

        $repository->insert($secondEntity);

        $this->assertNotEquals($entity->link, $secondEntity->link);
    }

    public function testUpdate()
    {
        $newsId = 1;
        $newsTitle = "first new";
        $newsLink = "first-new";

        DB::insert('INSERT INTO news (id, title, link) VALUES (:id, :title, :link)', [
            'id' => $newsId,
            'title' => $newsTitle,
            'link' => $newsLink,
        ]);

        $repository = $this->createRepository();
        $newsEntity = $repository->getByPk($newsId);

        $updatedTitle = "updated title";
        $newsEntity->title = $updatedTitle;

        $repository->update($newsEntity);
        $repository = $this->createRepository();
        $updatedEntity = $repository->getByPk($newsId);

        $this->assertEquals($updatedTitle, $updatedEntity->title, 'Title was successfully updated');


        $newEntity = $repository->createNewEntity();
        $result = $repository->update($newsEntity);

        $this->assertFalse($result, 'A new entity cannot be updated, it should be inserted first');
    }

    //todo: move into standalone entity test
    public function testIncreaseClicksCount()
    {
        $newsId = 1;
        $newsTitle = "first new";
        $newsLink = "first-new";

        DB::insert('INSERT INTO news (id, title, link, clicks_count) VALUES (:id, :title, :link, :clicks_count)', [
            'id' => $newsId,
            'title' => $newsTitle,
            'link' => $newsLink,
            'clicks_count' => 0,
        ]);

        $repository = $this->createRepository();
        $newsEntity = $repository->getByPk($newsId);

        $newsEntity->increaseClicksCount();

        $this->assertEquals(1, $newsEntity->clicks_count);
    }

    /**
     * @return NewsRepository|mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function createRepository()
    {
        return app()->make(NewsRepository::class);
    }
}
