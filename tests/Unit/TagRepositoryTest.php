<?php

namespace Tests\Unit;

use App\Core\Contract\Entity;
use App\Core\Contract\EntityCollection;
use App\Models\Tag\TagEntity;
use App\Models\Tag\TagRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

/**
 * It should be created and used seeders, etc in a proper way
 *
 * @package Tests\Unit
 */
class TagRepositoryTest extends TestCase
{
    use RefreshDatabase;

    public function testCreateEntity()
    {
        $repository = $this->createRepository();
        $entity = $repository->createNewEntity();

        $this->assertInstanceOf(TagEntity::class, $entity, 'It was created appropriate entity object');
        $this->assertNull($entity->id, 'The entity is new and does not have the primary key value');
    }

    /**
     * The test check that the getting entity by primary key works fine
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \PHPUnit\Framework\Exception
     * @throws \PHPUnit\Framework\ExpectationFailedException
     * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
     */
    public function testGetByPk(): void
    {
        $tagId = 1;
        $tagTitle = "first tag";
        $tagLink = "first-tag";

        $repository = $this->createRepository();
        $tagEntity = $repository->getByPk($tagId);

        $this->assertNull($tagEntity, 'It should be returned NULL because there is no records in the database');

        DB::insert('INSERT INTO tags (id, title, link) VALUES (:id, :title, :link)', [
            'id' => $tagId,
            'title' => $tagTitle,
            'link' => $tagLink,
        ]);
        $tagEntity = $repository->getByPk($tagId);

        $this->assertInstanceOf(TagEntity::class, $tagEntity, 'It was created appropriate entity object');
        $this->assertEquals($tagId, $tagEntity->id, 'The entity contains correct id');
        $this->assertEquals($tagTitle, $tagEntity->title, 'The entity contains correct title');
        $this->assertEquals($tagLink, $tagEntity->link, 'The entity contains correct link');
    }

    public function testGetList()
    {
        for ($i = 1; $i < 3; $i++) {
            DB::insert('INSERT INTO tags (id, title, link) VALUES (:id, :title, :link)', [
                'id' => $i,
                'title' => 'title_' . $i,
                'link' => 'link-' . $i,
            ]);
        }

        $repository = $this->createRepository();

        $listOfTags = $repository->getList();
        $this->assertInstanceOf(EntityCollection::class, $listOfTags, 'It was created a collection');

        $i = 1;
        foreach ($listOfTags as $tagEntity) {
            $this->assertInstanceOf(Entity::class, $tagEntity, 'It was created appropriate entity object');
            $this->assertEquals($i, $tagEntity->id, 'The entity contains correct id');
            $this->assertEquals("title_" . $i, $tagEntity->title, 'The entity contains correct title');

            $i++;
        }
    }

    public function testInsert()
    {
        $repository = $this->createRepository();

        $entity = $repository->createNewEntity();

        $entity->title = 'test title';
        $entity->link = 'test-link';

        $repository->insert($entity);

        $this->assertNotNull($entity->id, 'It was assigned the entity primary key value');

        $databaseEntity = $repository->getByPk($entity->id);
        $this->assertEquals($entity->title, $databaseEntity->title);
        $this->assertEquals($entity->link, $databaseEntity->link);
    }

    public function testUpdate()
    {
        $tagId = 1;
        $tagTitle = "first tag";
        $tagLink = "first-link";

        DB::insert('INSERT INTO tags (id, title, link) VALUES (:id, :title, :link)', [
            'id' => $tagId,
            'title' => $tagTitle,
            'link' => $tagLink,
        ]);

        $repository = $this->createRepository();
        $tagEntity = $repository->getByPk($tagId);

        $tagEntity->title = $updatedTitle = "updated title";
        $tagEntity->link = $updatedLink = "updated-link";

        $repository->update($tagEntity);
        $repository = $this->createRepository();
        $updatedEntity = $repository->getByPk($tagId);

        $this->assertEquals($updatedTitle, $updatedEntity->title, 'Title was successfully updated');
        $this->assertEquals($updatedLink, $updatedEntity->link, 'Link was successfully updated');


        $newEntity = $repository->createNewEntity();
        $result = $repository->update($newEntity);

        $this->assertFalse($result, 'A new entity cannot be updated, it should be inserted first');
    }

    /**
     * @return TagRepository|mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function createRepository()
    {
        return app()->make(TagRepository::class);
    }
}
