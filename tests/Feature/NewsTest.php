<?php

namespace Tests\Feature;

use App\Models\News\NewsEntity;
use App\Models\News\NewsRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NewsTest extends TestCase
{
    use RefreshDatabase;

    /** @var NewsEntity */
    private $newsEntity;

    public function setUp(): void
    {
        parent::setUp();

        $repository = $this->createRepository();
        $this->newsEntity = $repository->createNewEntity();

        $this->newsEntity->title = 'The first news title';
        $this->newsEntity->content = 'The first news content';

        $repository->insert($this->newsEntity);
    }

    public function testAUserCanGetListOfNews()
    {
        $response = $this->getJson('/api/news')->json();
        $this->assertCount(1, $response);
        $this->assertEquals($this->newsEntity->title, reset($response)['title']);
    }

    public function testAUserCanViewNews()
    {
        $response = $this->getJson('/api/news/' . $this->newsEntity->link)->json();
        $this->assertEquals($this->newsEntity->content, $response['content']);

        $repository = $this->createRepository();
        $databaseEntity = $repository->getByPk($this->newsEntity->id);

        $this->assertEquals($this->newsEntity->clicks_count + 1, $databaseEntity->clicks_count, 'The count of clicks was increased');
    }

    public function testAUserCanAddNews()
    {
        $newEntity = [
            'title' => 'Really cool news title',
            'content' => 'Really cool news content',
        ];

        $response = $this->postJson('/api/news', $newEntity)->json();

        $this->assertArrayHasKey('id', $response);
        $this->assertArrayHasKey('title', $response);
        $this->assertArrayHasKey('content', $response);
        $this->assertArrayHasKey('link', $response);
        $this->assertArrayHasKey('clicks_count', $response);

        $response = $this->getJson('/api/news/' . $response['link'])->json();
        $this->assertEquals($newEntity['content'], $response['content'], 'A news was saved and could be received by API');
    }

    public function testAUserCanUpdateNews()
    {
        $newEntity = [
            'title' => 'Updated news title',
            'content' => 'Updated news content',
        ];

        $response = $this->putJson('/api/news/' . $this->newsEntity->id, $newEntity)->json();

        $this->assertArrayHasKey('id', $response);
        $this->assertArrayHasKey('title', $response);
        $this->assertArrayHasKey('content', $response);
        $this->assertArrayHasKey('link', $response);
        $this->assertArrayHasKey('clicks_count', $response);

        $response = $this->getJson('/api/news/' . $response['link'])->json();
        $this->assertEquals($this->newsEntity->id, $response['id'], 'A news was updated without changing the id');
        $this->assertEquals($newEntity['title'], $response['title'], 'A news title was updated');
        $this->assertEquals($newEntity['content'], $response['content'], 'A news content was updated');
    }

    public function testAUserCanDeleteNews()
    {
        $this->deleteJson('/api/news/' . $this->newsEntity->id)->assertStatus(200);
        $this->getJson('/api/news/' . $this->newsEntity->link)->assertStatus(404);
    }

    /**
     * @return NewsRepository|mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function createRepository()
    {
        return app()->make(NewsRepository::class);
    }
}
