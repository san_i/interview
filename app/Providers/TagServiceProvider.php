<?php

namespace App\Providers;

use App\Models\Tag\TagFactory;
use App\Models\Tag\TagRepository;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\ServiceProvider;

class TagServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TagRepository::class, function ($app) {
            return new TagRepository(
                $app->get(DatabaseManager::class),
                $app->get(TagFactory::class),
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
