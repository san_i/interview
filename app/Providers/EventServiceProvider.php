<?php

namespace App\Providers;

use App\Core\Base\LinkGenerator;
use App\Models\News\Event\NewsRepositoryBeforeInsert;
use App\Models\Tag\Event\TagRepositoryBeforeInsert;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        NewsRepositoryBeforeInsert::class => [
            \App\Models\News\LinkGenerator::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
