<?php

namespace App\Providers;

use App\Models\News\NewsFactory;
use App\Models\News\NewsRepository;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\ServiceProvider;

class NewsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(NewsRepository::class, function ($app) {
            return new NewsRepository(
                $app->get(DatabaseManager::class),
                $app->get(NewsFactory::class),
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
