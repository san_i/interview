<?php

namespace App\Core\Event;

use App\Core\Base\Entity;
use Illuminate\Queue\SerializesModels;

/**
 * todo: create an interface
 * @package App\Core\Event
 */
abstract class RepositoryBeforeInsert
{
    use SerializesModels;

    public Entity $entity;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Entity $entity)
    {
        $this->entity = $entity;
    }
}
