<?php


namespace App\Core\Contract;

/**
 * This interface should be implemented by all entity factories
 *
 * @package App\Core\Contract
 */
interface EntityFactory
{
    public function create(array $attributes = []): Entity;

    public function createCollectionOfEntities(array $config);
}
