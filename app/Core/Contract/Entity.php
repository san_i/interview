<?php

namespace App\Core\Contract;

/**
 * It is an interface that should implement each entities in the system
 *
 * @package app\Core\Contract
 */
interface Entity
{
    public function setRawAttributes(array $attributes, $sync = false);
}
