<?php

namespace App\Core\Contract;

use Illuminate\Support\Collection;

/**
 * It is an interface of all repositories
 *
 * @package app\Core\Contract
 */
interface Repository
{
    public function getByPk($pk): ?Entity;

    public function getList($criteria): Collection;

    public function insert(Entity $entity): bool;

    public function update(Entity $entity): bool;

    public function createNewEntity(): Entity;
}
