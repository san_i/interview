<?php


namespace App\Core\Base;


use App\Core\Contract\EntityCollection;
use Illuminate\Support\Collection;

/**
 * It is a default collection implementation
 *
 * @package App\Core\Base
 */
class DefaultCollection extends Collection implements EntityCollection
{

}
