<?php


namespace App\Core\Base;


/**
 * The factory may be used for entities that do not require specific logic
 *
 * @package App\Core\Base
 */
class DefaultEntityFactory extends EntityFactory
{
    public function setEntityClassName(string $className): self
    {
        $this->entityClassName = $className;

        return $this;
    }
}
