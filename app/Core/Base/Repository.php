<?php

namespace App\Core\Base;

use App\Core\Contract\Entity;
use App\Core\Contract\EntityFactory;
use App\Core\Contract\Repository as IRepository;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Collection;

/**
 * It is a parent class of all repositories
 *
 * It is just a quick implementation.
 * The class could be extended with such stuff like IdentityMap, etc
 *
 * @package App\Core\Base
 */
abstract class Repository implements IRepository
{
    /**
     * @var DatabaseManager
     */
    protected DatabaseManager $db;
    protected string $pkFieldName = 'id';
    protected string $tableName;
    protected EntityFactory $entityFactory;

    public function __construct(DatabaseManager $db, EntityFactory $entityFactory)
    {
        $this->db = $db;
        $this->entityFactory = $entityFactory;
    }

    public function createNewEntity(): Entity
    {
        return $this->entityFactory->create();
    }

    public function getByPk($pk): ?Entity
    {
        $requestResult = $this->db->selectOne("SELECT * FROM {$this->tableName} WHERE {$this->pkFieldName} = :pk", ['pk' => $pk]);

        return $requestResult !== null ? $this->createEntity((array)$requestResult) : null;
    }

    public function getList($criteria = null): Collection
    {
        $entities = $this->db->select("SELECT * FROM {$this->tableName}");

        return $this->entityFactory->createCollectionOfEntities((array)$entities);
    }

    public function insert(Entity $entity): bool
    {
        $this->beforeInsert($entity);

        $entityId = $this->db->connection()
            ->table($this->tableName)
            ->insertGetId($entity->getAttributes());

        if ($entityId !== 0) {
            $entity->id = $entityId;

            return true;
        }

        return false;
    }

    public function update(Entity $entity): bool
    {
        if ($entity->id === null) {
            return false;
        }

        $attributes = $entity->getAttributes();
        unset($attributes['id']);

        return $this->db->connection()
            ->table($this->tableName)
            ->where(['id' => $entity->id])
            ->update($attributes);
    }

    protected function createEntity(array $attributes): Entity
    {
        $entity = $this->entityFactory->create($attributes);

        return $entity;
    }

    public function deleteById(int $id): bool
    {
        return $this->db->table($this->tableName)->delete($id);
    }

    protected function beforeInsert(Entity $entity)
    {

    }
}
