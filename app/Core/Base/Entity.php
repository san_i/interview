<?php

namespace App\Core\Base;

use App\Core\Contract\Entity as IEntity;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Concerns\HasAttributes;

/**
 * It is some base implementation with skipping all of the attributes cast methods etc
 *
 * @package App\Core\Base
 */
abstract class Entity implements IEntity, Arrayable
{
    use HasAttributes;

    public function __get($key)
    {
        return $this->getAttributes()[$key] ?? null;
    }

    public function __set($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    public function toArray()
    {
        return $this->getAttributes();
    }
}
