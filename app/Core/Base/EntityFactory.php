<?php


namespace App\Core\Base;


use App\Core\Contract\Entity;
use App\Core\Contract\EntityCollection;
use App\Core\Contract\EntityFactory as IEntityFactory;
use Illuminate\Container\Container;

/**
 * It is the base class of entity factories
 *
 * @package App\Core\Base
 */
abstract class EntityFactory implements IEntityFactory
{

    /**
     * @var Container
     */
    private Container $container;
    protected string $entityClassName;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function create(array $attributes = []): Entity
    {
        $entity = $this->container->get($this->entityClassName);
        $entity->setRawAttributes($attributes);

        return $entity;
    }

    /**
     * @param array $config
     * @return EntityCollection|DefaultCollection
     */
    public function createCollectionOfEntities(array $config): EntityCollection
    {
        /** @var DefaultCollection $collection */
        $collection = $this->container->get(EntityCollection::class);

        foreach ($config as $attributes) {
            $entity = $this->create((array) $attributes);
            $collection->add($entity);
        }

        return $collection;
    }
}
