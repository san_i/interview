<?php


namespace App\Models\Tag;


use App\Core\Base\Entity as BaseEntity;

/**
 * Represent the news entity
 *
 * @property integer $id
 * @property string $title
 * @property string $link
 *
 * @package App\Models\News
 */
class TagEntity extends BaseEntity
{
}
