<?php

namespace App\Models\Tag;

use App\Core\Base\Repository;
use App\Models\Tag\Event\TagRepositoryBeforeInsert;

/**
 * The class represent repository of news
 *
 * @method TagEntity createNewEntity()
 * @method TagEntity getByPk($pk)
 * @method TagEntity[] getList()
 *
 * @package App\Models\News
 */
class TagRepository extends Repository
{
    protected string $tableName = 'tags';

    public function getByLink($link): ?TagEntity
    {
        $requestResult = $this->db->selectOne("SELECT * FROM {$this->tableName} WHERE link = :link", ['link' => $link]);

        return $requestResult !== null ? $this->createEntity((array)$requestResult) : null;
    }
}
