<?php


namespace App\Models\Tag;


use App\Core\Base\EntityFactory;
use Illuminate\Support\Str;

/**
 * The class responsible for the news entity creation
 *
 * @package App\Models\News
 */
class TagFactory extends EntityFactory
{
    protected string $entityClassName = TagEntity::class;

    public function fillLink(TagEntity $entity, bool $force = false): void
    {
        if ($entity->link === null || $force) {
            $entity->link = Str::slug($entity->title);
        }
    }
}
