<?php


namespace App\Models\News;


use App\Core\Base\Entity as BaseEntity;

/**
 * Represent the news entity
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $link
 * @property int $clicks_count
 * @property string $created_at
 * @property string $updated_at
 *
 * @package App\Models\News
 */
class NewsEntity extends BaseEntity
{

    public function increaseClicksCount(): void
    {
        $this->clicks_count++;
    }
}
