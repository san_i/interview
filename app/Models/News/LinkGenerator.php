<?php

namespace App\Models\News;

use App\Core\Base\Entity;
use App\Core\Event\RepositoryBeforeInsert;
use Illuminate\Support\Str;

class LinkGenerator
{
    private NewsRepository $newsRepository;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    public function handle(RepositoryBeforeInsert $event)
    {
        $entity = $event->entity;
        if ($entity->link === null) {
            $entity->link = $this->generateLink($entity);
        }
    }

    protected function generateLink(Entity $entity): string
    {
        $link = Str::slug($entity->title);

        $existedEntityWithLink = $this->newsRepository->getByLink($link);
        if ($existedEntityWithLink !== null) {
            $link = Str::random(10) . $link;
        }

        return $link;
    }
}
