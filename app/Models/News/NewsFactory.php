<?php


namespace App\Models\News;


use App\Core\Base\EntityFactory;
use App\Core\Contract\Entity;
use Illuminate\Support\Str;

/**
 * The class responsible for the news entity creation
 *
 * @package App\Models\News
 */
class NewsFactory extends EntityFactory
{
    protected string $entityClassName = NewsEntity::class;

    public function create(array $attributes = []): Entity
    {
        $attributes['clicks_count'] = $attributes['clicks_count'] ?? 0;

        return parent::create($attributes);
    }

    public function fillLink(NewsEntity $entity, bool $force = false): void
    {
        if ($entity->link === null || $force) {
            //todo: may be refactored
            $entity->link = Str::slug($entity->title);
        }
    }
}
