<?php

namespace App\Models\News;

use App\Core\Base\Repository;
use App\Core\Contract\Entity;
use App\Models\News\Event\NewsRepositoryBeforeInsert;

/**
 * The class represent repository of news
 *
 * @method NewsEntity createNewEntity()
 * @method NewsEntity getByPk($pk)
 * @method NewsEntity[] getList()
 *
 * @package App\Models\News
 */
class NewsRepository extends Repository
{
    protected string $tableName = 'news';

    public function getByLink($link): ?NewsEntity
    {
        $requestResult = $this->db->selectOne("SELECT * FROM {$this->tableName} WHERE link = :link", ['link' => $link]);

        return $requestResult !== null ? $this->createEntity((array)$requestResult) : null;
    }

    public function deleteById(int $id): bool
    {
        return $this->db->table($this->tableName)->delete($id);
    }

    protected function beforeInsert(Entity $entity)
    {
        parent::beforeInsert($entity);

        event(new NewsRepositoryBeforeInsert($entity));
    }
}
