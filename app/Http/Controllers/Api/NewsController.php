<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\News\NewsRepository;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NewsController extends Controller
{
    /**
     * @var NewsRepository
     */
    private NewsRepository $newsRepository;

    public function __construct(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    /**
     * Display a listing of the news.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->newsRepository->getList());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
        ]);

        $newsEntity = $this->newsRepository->createNewEntity();

        $newsEntity->title = $request->post('title');
        $newsEntity->content = $request->post('content');

        if ($this->newsRepository->insert($newsEntity)) {
            return response()->json($newsEntity);
        }

        throw new BadRequestHttpException('Item was not saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($link)
    {
        $newsEntity = $this->newsRepository->getByLink($link);

        if ($newsEntity === null) {
            throw new NotFoundHttpException('The link is broken');
        }

        $newsEntity->increaseClicksCount();
        $this->newsRepository->update($newsEntity);

        return response()->json($newsEntity);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
        ]);

        $newsEntity = $this->newsRepository->getByPk($id);

        if ($newsEntity === null) {
            throw new NotFoundHttpException('A news was not found');
        }

        $newsEntity->title = $request->post('title', $newsEntity->title);
        $newsEntity->content = $request->post('content', $newsEntity->content);

        if ($this->newsRepository->update($newsEntity)) {
            return response()->json($newsEntity);
        }

        throw new BadRequestHttpException('Item was not saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->newsRepository->deleteById($id);

        return response()->json();
    }
}
