<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('news', 'App\Http\Controllers\Api\NewsController@index');
Route::get('news/{link}', 'App\Http\Controllers\Api\NewsController@show');
Route::post('news', 'App\Http\Controllers\Api\NewsController@store');
Route::put('news/{id}', 'App\Http\Controllers\Api\NewsController@update');
Route::delete('news/{id}', 'App\Http\Controllers\Api\NewsController@destroy');
